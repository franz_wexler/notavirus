import tkinter as tk
import tkinter.ttk as ttk
import re as re
import os as os
import random as rand
import ctypes as c
import argparse as arg
import subprocess as sub

import winpriority as prior


MAX_WINDOWS = 8
INCREASE_PRIORITY = 0


def sval(x, y, w, h):
    return "{w}x{h}+{x}+{y}".format(x=int(x), y=int(y), w=int(w), h=int(h))


def gval(val):
    m = tuple(map(int, re.match("(\d+)x(\d+)([-+]\d+)([-+]\d+)", val).groups()))
    return m[2], m[3], m[0], m[1]


def randommultiplier(tolerance, step=0.1):
    return rand.randint(0, int((1 + tolerance - (1 - tolerance)) / step)) * step + (1 - tolerance)


class JumpingWindow:
    def __init__(self, master=None, x=None, y=None):
        if master is None:
            self._isroot = True
            self.win = tk.Tk()

            c.windll.shell32.SetCurrentProcessExplicitAppUserModelID('not.a.virus.1')
            self.master = self.win

            global ERRORIMAGE
            ERRORIMAGE = tk.PhotoImage(master=self.win, file='./erroricon.gif')
        else:
            self._isroot = False
            self.master = master
            self.win = tk.Toplevel(master)
        self.win.withdraw()  # hide window before creating widgets
        self.win.minsize(464, 140)
        self.win.maxsize(464, 140)
        self.win.resizable(width='false', height='false')
        self.win.transient(master)
        self.win.iconbitmap('error.ico')
        self.win.title("EXPLORER.EXE - Application Error")

        self.white_frame = tk.Frame(self.win, bg='#FFF')
        self.white_frame.pack(fill='x')

        self.upper_frame = tk.Frame(self.white_frame, bg='#FFF', height=90)
        self.upper_frame.pack_propagate(False)
        self.upper_frame.pack(fill='x')

        self.errorimage = tk.Label(self.upper_frame, bg='#FFF', image=ERRORIMAGE)
        self.errorimage.pack(side='left', padx=10, pady=10)

        self.label = tk.Label(self.upper_frame, bg='#FFF', anchor='w', justify='left', text=rand.choice(commentary))
        self.label.pack(fill='x', side='left', pady=10)

        self.bottom_frame = tk.Frame(self.white_frame, bg='#F0F0F0', height=50)
        self.bottom_frame.pack_propagate(False)
        self.bottom_frame.pack(fill='x', side='bottom')
        self.button = ttk.Button(self.bottom_frame, width=13, command=self.activate, text="OK")
        self.button.pack(side='right', padx=10, pady=10)
        self.button.focus_set()

        self.win.update()  # update to let tkinter compute size of window

        self.x, self.y, self.w, self.h = gval(self.win.geometry())
        self.minx, self.miny = 0, 0
        self.maxx, self.maxy = self.win.winfo_screenwidth() - self.w, self.win.winfo_screenheight() - self.h

        if x is not None:
            self.x = x
        if y is not None:
            self.y = y
        self.updatepos()

        self.active = False

        self.velx, self.vely = 0.6, 0
        self.maxvelx = 3

        self.gravity = 0.03

        self.borderbouncemultiplier = 0.8
        self.borderescapevel = 1.5
        self.borderescapejump = 6

        self.mouseescapevelx, self.mouseescapevely = 3, 3
        self.mouseescapeignoretime = 60
        self.mouseescapejump = 4

        self.randomfunc = lambda: randommultiplier(0.1)

        self._ignoretime = 0

        self.win.bind('<Enter>', self.mouse_enter)  # bind events at end of function to avoid Tk errors
        self.win.bind('<Return>', self.activate)

        self.win.deiconify()  # show window
        self.win.focus_force()

        #   W I N D O W   B E H A V I O R
        # -----=======================-----
        # if `active`
        #   moves by `velx` in x-axis
        #   if is on edge of screen;
        #      bounces (`velx` = -`velx`)
        #   if `velx` is greater than `maxvelx`
        #      `gravity` (yes, gravity) is subtracted/added to `velx` in direction of 0;
        #   move window by `vely` in y-axis;
        #   vely is accelerated by `gravity`;
        #   if is on bottom of screen
        #     `vely` is multiplied by -`borderbouncemultiplier`;
        #     if `vely` is smaller than `borderescapevel`
        #       `borderescapejump` is subtracted from `vely`;
        #   if mouse is over the window
        #     if `_ignoretime` is 0
        #       change values of `velx` and `vely` depending on relative position of mouse using `mouseescapevelx` and
        #       `mouseescapevely`;
        #       if window is on bottom of screen
        #         `mouseescapejump` is subtracted from `vely`;
        #       `_ignoretime` is set to `mouseescapeignoretime`;
        #   if `_ignoretime` is greater than 0
        #      1 is subtracted from `_ignoretime`;
        # `borderbouncemultiplier`, `borderescapejump`, `mouseescapevelx` and `mouseescapevely` are always multiplied
        # by call of `randomfunc`

    def update(self):
        if self._ignoretime > 0:
            self._ignoretime -= 1
        if self.active:
            self.x += self.velx

            if self.velx > self.maxvelx:
                self.velx -= self.gravity
            elif self.velx < -self.maxvelx:
                self.velx += self.gravity

            if self.x < self.minx:
                self.x = self.minx
                self.velx = -self.velx * self.randomfunc()
            if self.x > self.maxx:
                self.x = self.maxx
                self.velx = -self.velx * self.randomfunc()

            self.y += self.vely
            self.vely += self.gravity

            if self.y < self.miny:
                self.y = self.miny
                self.vely = -self.vely
            if self.y > self.maxy:
                self.y = self.maxy
                self.vely = -self.vely * self.borderbouncemultiplier * self.randomfunc()

            if abs(self.vely) < self.borderescapevel and self.y >= self.maxy:
                self.vely = -self.borderescapejump * self.randomfunc()

            self.updatepos()
        if self._isroot:
            try:
                self.win.update()
            except tk.TclError:
                # Application has been destroyed... maybe it was other error... who knows?
                exit(0)

    def updatepos(self):
        try:
            self.win.geometry(sval(self.x, self.y, self.w, self.h))
        except tk.TclError:
            pass

    def mouse_enter(self, event):
        if self.active and self._ignoretime <= 0:
            if event.x < self.w / 2 - 8:  # left half
                self.velx = self.mouseescapevelx * self.randomfunc()
            elif event.x > self.w / 2 + 8:
                self.velx = -self.mouseescapevelx * self.randomfunc()

            self._ignoretime = self.mouseescapeignoretime
            self.vely = -self.mouseescapejump * self.randomfunc()
            if self.y >= self.maxy - self.h:
                self.vely = -self.mouseescapejump

    def activate(self, event=None):
        if self.active:
            self.clone()
        self.active = True

    def clone(self):
        if len(jumping_windows) >= MAX_WINDOWS:
            sub.Popen(['PYTHON', 'main.py', '-x', str(int(self.x)), '-y', str(int(self.y)), '-a'])
            # os.system('PYTHON main.py -x {} -y {} -a'.format(int(self.x), int(self.y)))
        else:
            newwindow = JumpingWindow(self.master, self.x, self.y)
            newwindow.velx = -self.velx
            newwindow.active = True
            jumping_windows.append(newwindow)


with open('commentary.txt') as f:
    commentary = str(f.read()).split('\n---\n')


parser = arg.ArgumentParser()
parser.add_argument('-x', '--x', help="beginning x position value", type=int)
parser.add_argument('-y', '--y', help="beginning y position value", type=int)
parser.add_argument('-a', '--active', help="should the window be initially jumping", action='store_true')
parser.add_argument('--priority', default=INCREASE_PRIORITY, choices=range(-2, 3+1))
parsedargs = parser.parse_args()


print("Increasing priority to {}".format(parsedargs.priority))
prior.set_priority(prior.priority_values[parsedargs.priority])


jumping_windows = [JumpingWindow(x=parsedargs.x, y=parsedargs.y)]
if parsedargs.active:
    jumping_windows[0].activate()


while True:
    for j in jumping_windows:
        j.update()
